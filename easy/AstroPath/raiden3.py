

# _ _ _ _ _ A S T R O _ P A T H _ _ _ _ _  #
# - - - - - - - - - - - - - - - - - - - - -#


# Written by Blake Smith
# This is a Pygame experiment and a learn as I go type of thing.
# AstroPath is modeled after the arcade game Raiden.

import pygame
import random


white = (255,255,255)
black = (0,0,0)
red = (255,0,0)
green = (0,255,0)
blue = (0,0,255)



# # # # # # # # # - - CLASSES - - # # # # # # # # #

class Block(pygame.sprite.Sprite):

    def __init__(self,color):
        # Init the parent class
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('easy/AstroPath/astroid-1.png')
        self.rect = self.image.get_rect()



        #self.image = pygame.Surface((50,50)).convert()
        #self.image.fill(pygame.Color("white"))
        #self.rect = self.image.get_rect()

class Player(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('easy/AstroPath/starshipOne.png')
        self.rect = self.image.get_rect()
        

        #self.image = pygame.Surface((50,50)).convert()
        #self.image.fill(pygame.Color("green"))
        #self.rect = self.image.get_rect()


class Bullet(pygame.sprite.Sprite):

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.Surface([4,10])
        self.image.fill(green)

        self.rect = self.image.get_rect()

    def update(self):
        self.rect.y -= 30




# Initialize Pygame - - - - - - - -
pygame.init()

# Set up screen dimensions - - - - - - - -
screen_width = 800
screen_height = 600
screen = pygame.display.set_mode([screen_width, screen_height])

# Game title display
pygame.display.set_caption('Astro Path')


# Create SPRITES LIST - - - - - - - - -
all_sprites_list = pygame.sprite.Group()
block_list = pygame.sprite.Group()
bullet_list = pygame.sprite.Group()
astroid_trash = pygame.sprite.Group()



# Create the PLAYER - - - - - - - - - -
player = Player()
all_sprites_list.add(player)



# Game paramaters - - - - - - - - - - -
clock = pygame.time.Clock()

points = 0

# Blit SCORE to the screen - - - - - - - - - -
smallFont = pygame.font.SysFont('Charcoal', 25)
mediumFont = pygame.font.SysFont('Charcoal', 30)
largeFont = pygame.font.SysFont('Charcoal', 50)

def score(points):
    text = largeFont.render(str(points), True, green)
    screen.blit(text, [750,510])



# # # # # # # # # # - - MAIN GAME LOOP - - # # # # # # # # #

class gameLoop():

    def __init__(self):

        # GENERATED THE ASTROIDS - - - - - - - - - - -
        self.block = Block(black)
        self.reset()

    def reset(self):
        self.points = 0
        self.x_change = 0
        self.y_change = 0
        self.movement = 11
        self.astroid_speed = 10
        player.rect.x = 300
        player.rect.y = 400
        
        self.genRoid()
        block_list.remove(self.block)
        all_sprites_list.remove(self.block)
        astroid_trash.add(self.block)
        

        # for DQN
        self.reward = 0
        self.done = False

    def genRoid(self):
            
        print ('GENERATION!!!')
        self.block.rect.x = random.randrange(screen_width * 0.8)
        self.block.rect.y = -200
        
        #all_sprites_list.add(self.block)
        #block_list.add(self.block)  

    def run(self, input_actions):
             
        if self.done == True:
            self.reset()
  
        self.reward = -0.1

        print(input_actions)
        # MOVEMENT - - - - - - - - - - - 
        if input_actions[1] == 1: # LEFT
            self.x_change -= self.movement
        if input_actions[2] == 1: # RIGHT
            self.x_change += self.movement
        if input_actions[3] == 1: #UP
            self.y_change -= self.movement
        if input_actions[4] == 1: # DOWN
            self.y_change += self.movement
          
        # CONTINUAL MOVEMENT - - - - - - - - - - -      
        if input_actions[1] == 0 and input_actions[2] == 0:
            self.x_change = 0
        if input_actions[3] == 0 and input_actions[4] == 0:
            self.y_change = 0



        # BULLET FIRE - - - - - - - - - - -
        if input_actions[5] == 1:
            bullet = Bullet()

            bullet.rect.x = player.rect.x + 38
            bullet.rect.y = player.rect.y

            all_sprites_list.add(bullet)
            bullet_list.add(bullet)


                            

        player.rect.x += self.x_change
        player.rect.y += self.y_change
        all_sprites_list.update()



        for bullet in bullet_list:

            block_hit_list = pygame.sprite.spritecollide(bullet, block_list, True)

            for block in block_hit_list:
                bullet_list.remove(bullet)
                all_sprites_list.remove(bullet)
                self.points += 1
                self.reward = 100
                block_list.remove(self.block)
                astroid_trash.add(self.block)
                
                print ('points: ', self.points)

            

            if bullet.rect.y < -10:
                bullet_list.remove(bullet)
                all_sprites_list.remove(bullet)

                    

        if self.block.rect.y > 600:
            self.done = True
            self.reward = -1

                            

        if len(astroid_trash) == 1 :
            self.block.rect.x = random.randrange(screen_width * 0.8)
            self.block.rect.y = -200

            all_sprites_list.add(self.block)
            block_list.add(self.block)
            astroid_trash.remove(self.block)

               
        if self.block.rect.x < player.rect.x and input_actions[1] == 1: # LEFT
            self.reward = 1

        
        if self.block.rect.x > player.rect.x and input_actions[2] == 1: # RIGHT
            self.reward = 1

        if self.block.rect.x+30 > player.rect.x and self.block.rect.x-30 < player.rect.x and input_actions[5] == 1: # FIRE
            self.reward = 1
     

       # Astroid collision detection - - - - - - - - - -
        block_collision_list = pygame.sprite.spritecollide(player, block_list, True)
        
        for block in block_collision_list:
            self.done = True
            self.reward = -1
            


        # END astroid collision test
        self.block.rect.y += self.astroid_speed

        # GAME BOUNDARIES - - - - - - - - -

        if player.rect.x < 0 or player.rect.x > screen_width - 79:
            
            self.done = True
            self.reward = -1
                
        if player.rect.y < 0 or player.rect.y > screen_height - 70:
            self.done = True
            self.reward = -1
                    
        screen.fill(black)
        score(self.points)
        
        gui = pygame.image.load('easy/AstroPath/GUI_test.png')
        screen.blit(gui, [0,0])
        
        all_sprites_list.draw(screen)

        pygame.display.update()

        clock.tick(30)

        image_data = pygame.surfarray.array3d(pygame.display.get_surface())

        print("reward", self.reward)
        return image_data, self.reward, self.done
             

#a = gameLoop()

#while True:
#    a.run([0,0,0,0,0,])

