import cv2
import sys
from BrainDQN_Nature import BrainDQN
import numpy as np
sys.path.append("easy/AstroPath/")
import raiden3 as game
sys.path.append("hard/sky_eraser/")
import sky_eraser as hard_game


# preprocess raw image to 80*80 gray image
def preprocess(observation):
    observation = cv2.cvtColor(cv2.resize(observation, (80, 80)), cv2.COLOR_BGR2GRAY)
    ret, observation = cv2.threshold(observation,1,255,cv2.THRESH_BINARY)
    return np.reshape(observation,(80,80,1))

def playRaiden():
    
    # Step 1: init BrainDQN
    actions = 6 # 6 state: nothing, left, right, up, down, fire
    brain = BrainDQN(actions)
    # Step 2: init raiden Game
    #raiden = game.gameLoop()
    raiden = hard_game.gameLoopHard()

    # Step 3: play game
    # Step 3.1: obtain init state
    action0 = np.array([1,0,0,0,0,0])  # do nothing
    observation0, reward0, terminal = raiden.run(action0)
    observation0 = cv2.cvtColor(cv2.resize(observation0, (80, 80)), cv2.COLOR_BGR2GRAY)
    ret, observation0 = cv2.threshold(observation0,1,255,cv2.THRESH_BINARY)
    brain.setInitState(observation0)

    # Step 3.2: run the game
    while 1!= 0:
        action = brain.getAction()
        nextObservation,reward,terminal = raiden.run(action)
        nextObservation = preprocess(nextObservation)
        brain.setPerception(nextObservation,action,reward,terminal)

def main():
    playRaiden()

if __name__ == '__main__':
    main()
