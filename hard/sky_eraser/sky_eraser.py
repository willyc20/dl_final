#!/usr/bin/env python2

"""
Sky Eraser (For real this time!)
By Prerisoft

Execution script
File creation date: Jan 07, 2013

THIS FILE IS LICENSED UNDER THE 3-CLAUSE BSD LICENSE. PLEASE SEE LICENSE(.txt)
FOR DETAILS.
"""

import sys
import pygame
import constants, data, video, audio, control, scene, settings

memorial = "In memory of Elizabeth McClung, who fought to live life against \
the overwhelming inevitability of terminal illness."

def bootstrap():
    """
    Loads the title menu in a manner that keeps the scene data from being
    persistent in memory.
    """
    import title
    scene.load_scene(title.TitleScene, block=True)

def main():
    """
    Main routine and loop. Let's go!
    """
    video.create_window() # Make our game window
    control.init() # Set blocked events, hide the cursor
    control.set_exit_callback(sys.exit) # Set a basic exit callback
    #bootstrap() # Load our scene

    import game
    scene.load_scene(game.GameScene)

    while 1:
        control.pump() # Update control states
        scene.update() # Update active scene state
        video.render_all() # Put it all onscreen!


class gameLoopHard():

    def __init__(self):
        settings.global_init()
        video.create_window() # Make our game window
        control.init() # Set blocked events, hide the cursor
        control.set_exit_callback(sys.exit) # Set a basic exit callback
        #bootstrap() # Load our scene

        import game
        scene.load_scene(game.GameScene)

    def run(self, input_actions):
        control.pump(input_actions) # Update control states
        scene.update() # Update active scene state
        video.render_all() # Put it all onscreen!

        image_data = pygame.surfarray.array3d(pygame.display.get_surface())

        if settings.reward == -1:
            now_reward = -1
            settings.reward = 0.1
        else:
            now_reward = 0.1

        print(now_reward)

        return image_data, now_reward, True


#a = gameLoop()

#while True:
#    a.run([0,0,0,0,0,1])
